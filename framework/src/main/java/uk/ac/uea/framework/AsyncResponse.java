package uk.ac.uea.framework;

import org.json.JSONArray;

/**
 * Created by qrk13cdu on 30/11/2015.
 */
public interface AsyncResponse {
    void processFinish(JSONArray output);
}
