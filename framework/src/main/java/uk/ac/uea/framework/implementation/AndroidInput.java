package uk.ac.uea.framework.implementation;

import java.util.List;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;

import uk.ac.uea.framework.Input;
/**
 * Handles input of touch events on screen. Implements Interface Input.
 * Contains a member TouchHandler.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see TouchHandler
 */
public class AndroidInput implements Input {    
    TouchHandler touchHandler;

    /**
     * Creates AndroidInput Object. Depending on the version of SDK of the device/app, creates either
     * a SingleTouchHandler or MultiTouchHandler.
     * @param context Context, unused in this constructor.
     * @param view View, used to handle the rendering of the view.
     * @param scaleX Float, ratio of x axis coordinate system and screen width.
     * @param scaleY Float, ratio of y axis coordinate system and screen height.
     * @see SingleTouchHandler
     * @see MultiTouchHandler
     */
    public AndroidInput(Context context, View view, float scaleX, float scaleY) {
        if(Integer.parseInt(VERSION.SDK) < 5) 
            touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
        else
            touchHandler = new MultiTouchHandler(view, scaleX, scaleY);        
    }

    /**
     * Checks if a TouchEvents id is currently touched down.
     * @param pointer int id into a touch event pool.
     * @return Boolean True if down, false otherwise.
     * @see com.kilobolt.framework.Input.TouchEvent
     */
    @Override
    public boolean isTouchDown(int pointer) {
        return touchHandler.isTouchDown(pointer);
    }
    /**
     * Retrieves the x coordinate of a TouchEvent.
     * @param pointer int id into a touch event pool.
     * @return int representation of x coordinate.
     * @see com.kilobolt.framework.Input.TouchEvent
     */
    @Override
    public int getTouchX(int pointer) {
        return touchHandler.getTouchX(pointer);
    }
    /**
     * Retrieves the y coordinate of a TouchEvent.
     * @param pointer int id into a touch event pool.
     * @return int representation of y coordinate.
     * @see com.kilobolt.framework.Input.TouchEvent
     */
    @Override
    public int getTouchY(int pointer) {
        return touchHandler.getTouchY(pointer);
    }


    /**
     * Retrieves all the occurrences of a touch, as a list of TouchEvents.
     * @return List<TouchEvent> Record of every touch.
     * @see com.kilobolt.framework.Input.TouchEvent
     */
    @Override
    public List<TouchEvent> getTouchEvents() {
        return touchHandler.getTouchEvents();
    }
    
}
