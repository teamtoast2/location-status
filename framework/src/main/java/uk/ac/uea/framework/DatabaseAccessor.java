package uk.ac.uea.framework;

import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ConcurrentModificationException;
import java.util.concurrent.ExecutionException;

/**
 * Created by qrk13cdu on 27/11/2015.
 */
public class DatabaseAccessor extends AsyncTask<String, Void, JSONArray>  {

    @Override
    protected JSONArray doInBackground(String... params) {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://10.0.2.2:5432/studentdb",
                    "student",
                    "dbpassword"
            );

        } catch (Exception e) {
            System.out.println("Joe Error  " + e);

        }
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    params[0]);

            ResultSet resultSet = pstmt.executeQuery();

            JSONArray jsonArray = new JSONArray();

            while (resultSet.next()) {
                int total_rows = resultSet.getMetaData().getColumnCount();
                JSONObject obj = new JSONObject();
                for (int i = 0; i < total_rows; i++) {
                    obj.put(resultSet.getMetaData().getColumnLabel(i + 1)
                            .toLowerCase(), resultSet.getObject(i + 1));

                }
                jsonArray.put(obj);

            }
            connection.close();
            return jsonArray;

        } catch (Exception ex) {
        }


        return null;
    }
}
