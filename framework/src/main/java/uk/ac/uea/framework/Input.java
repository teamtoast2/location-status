package uk.ac.uea.framework;

import java.util.List;
/**
 * Interface, Handles input of touch events on screen. See implementation {@link uk.ac.uea.framework.implementation.AndroidFileIO}
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 */
public interface Input {

    /**
     * Static inner class, Handles the location and type of touch on screen.
     * @author Joe Lilley
     * @since 2015-10-22
     * @version 1.0
     */
    public static class TouchEvent {
        public static final int TOUCH_DOWN = 0;
        public static final int TOUCH_UP = 1;
        public static final int TOUCH_DRAGGED = 2;
        public static final int TOUCH_HOLD = 3;

        public int type;
        public int x, y;
        public int pointer;


    }

    public boolean isTouchDown(int pointer);

    public int getTouchX(int pointer);

    public int getTouchY(int pointer);

    public List<TouchEvent> getTouchEvents();
}
