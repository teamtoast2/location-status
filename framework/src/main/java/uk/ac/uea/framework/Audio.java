package uk.ac.uea.framework;
import uk.ac.uea.framework.implementation.AndroidAudio;
/**
 * Interface Handles the creation of Audio, in the form of Music or Sound. See implementation {@link AndroidAudio}.
 * @author Joe Lilley
 * @since 2015-10-26
 * @version 1.0
 */
public interface Audio {
    public Music createMusic(String file);

    public Sound createSound(String file);
}
