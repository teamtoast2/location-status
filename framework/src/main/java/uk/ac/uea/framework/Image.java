package uk.ac.uea.framework;

import uk.ac.uea.framework.Graphics.ImageFormat;
/**
 * Interface, provides header methods for image manipulation. See implementation{@link uk.ac.uea.framework.implementation.AndroidImage}
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 */
public interface Image {
    public int getWidth();
    public int getHeight();
    public ImageFormat getFormat();
    public void dispose();
}
