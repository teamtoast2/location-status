package uk.ac.uea.framework.implementation;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Handles the rendering of the view in an AndroidGame. Extends SurfaceView and implements Runnable.
 * Contains an AndroidGame, Bitmap, Thread and SurfaceHolder.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see AndroidGame
 * @see Bitmap
 * @see Thread
 * @see SurfaceHolder
 * @see SurfaceView
 * @see Runnable
 */
public class AndroidFastRenderView extends SurfaceView implements Runnable {
    AndroidGame game;
    Bitmap framebuffer;
    Thread renderThread = null;
    SurfaceHolder holder;
    volatile boolean running = false;

    /**
     * Creates an AndroidFastRenderView Object and instantiates a SurfaceView by super call.
     * Super method call to store reference of the SurfaceHolder.
     * @param game AndroidGame Singleton Object of which controls the game, stores this reference.
     * @param framebuffer Bitmap containing the coordinate dimensions of screen, handles Portrait and Landscape,
     *                    stores this reference.
     * @see AndroidGame
     * @see Bitmap
     * @see SurfaceHolder
     * @see SurfaceView
     */
    public AndroidFastRenderView(AndroidGame game, Bitmap framebuffer) {
        super(game);
        this.game = game;
        this.framebuffer = framebuffer;
        this.holder = getHolder();

    }

    /**
     * Resumes the current View, creates new thread of current instance and starts it.
     */
    public void resume() { 
        running = true;
        renderThread = new Thread(this);
        renderThread.start();   

    }

    /**
     * Run method manages the current View. Handles frame rate to allow smooth movement on screen,
     * independent of how many resources allocated to the application. Calculates time between
     * each frame to update the screen.
     * @see AndroidGame
     * @see Canvas
     * @see SurfaceHolder
     */
    public void run() {
        Rect dstRect = new Rect();
        long startTime = System.nanoTime();
        while(running) {  
            if(!holder.getSurface().isValid())
                continue;           
            

            float deltaTime = (System.nanoTime() - startTime) / 10000000.000f;
            startTime = System.nanoTime();
            
            if (deltaTime > 3.15){
                deltaTime = (float) 3.15;
           }
     

            game.getCurrentScreen().update(deltaTime);
            game.getCurrentScreen().paint(deltaTime);
          
            
            
            Canvas canvas = holder.lockCanvas();
            canvas.getClipBounds(dstRect);
            canvas.drawBitmap(framebuffer, null, dstRect, null);                           
            holder.unlockCanvasAndPost(canvas);
            
            
        }
    }

    /**
     * Pause the current View, pausing the thread by the join method.
     * @see Thread
     */
    public void pause() {                        
        running = false;                        
        while(true) {
            try {
                renderThread.join();
                break;
            } catch (InterruptedException e) {
                // retry
            }
            
        }
    }     
    
  
}
