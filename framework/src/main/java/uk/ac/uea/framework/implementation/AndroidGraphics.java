package uk.ac.uea.framework.implementation;

import java.io.IOException;
import java.io.InputStream;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;

import uk.ac.uea.framework.Graphics;
import uk.ac.uea.framework.Image;
/**
 * Manages drawing of objects on screen. Implements Interface Graphics.
 * Contains a AssetManager, Bitmap, Canvas, Paint and two Rect's.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see AssetManager
 * @see Bitmap
 * @see AndroidFastRenderView
 * @see Graphics
 * @see Canvas
 * @see Paint
 * @see Rect
 */
public class AndroidGraphics implements Graphics {
    AssetManager assets;
    Bitmap frameBuffer;
    Canvas canvas;
    Paint paint;
    Rect srcRect = new Rect();
    Rect dstRect = new Rect();

    /**
     * Creates an AndroidGraphics object. Initialises the Canvas using the framebuffer. Also
     * initialises the Paint object.
     * @param assets AssetManager containing assets such as Images to use in the game.
     * @param frameBuffer Bitmap containing the coordinate system.
     * @see Canvas
     * @see Paint
     */
    public AndroidGraphics(AssetManager assets, Bitmap frameBuffer) {
        this.assets = assets;
        this.frameBuffer = frameBuffer;
        this.canvas = new Canvas(frameBuffer);
        this.paint = new Paint();
    }

    /**
     * Loads an image from file. Based upon a config file to load the Image in the correct format.
     * @param fileName String representation of the file name in the assests folder in the project root.
     * @param format ImageFormat containing the format of the Image being loaded. Represented as an Enum.
     * @return AndroidImage of the loaded Image.
     * @see Image
     * @see AndroidImage
     */
    @Override
    public Image newImage(String fileName, ImageFormat format) {
        Config config = null;
        if (format == ImageFormat.RGB565)
            config = Config.RGB_565;
        else if (format == ImageFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;

        Options options = new Options();
        options.inPreferredConfig = config;
        
        
        InputStream in = null;
        Bitmap bitmap = null;
        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Couldn't load bitmap from asset '"
                        + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load bitmap from asset '"
                    + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }

        if (bitmap.getConfig() == Config.RGB_565)
            format = ImageFormat.RGB565;
        else if (bitmap.getConfig() == Config.ARGB_4444)
            format = ImageFormat.ARGB4444;
        else
            format = ImageFormat.ARGB8888;

        return new AndroidImage(bitmap, format);
    }

    /**
     * Clears the colour of the canvas based upon an Integer each int representing a colour.
     * {@link android.graphics.Color}  Bitwise operations performed in body of method.
     * @param color int representing a colour in {@link android.graphics.Color}.
     */
    @Override
    public void clearScreen(int color) {
        canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8,
                (color & 0xff));
    }

    /**
     * Draws a line on screen of a certain colour. {@link android.graphics.Color}
     * @param x int starting x coordinate
     * @param y int starting y coordinate
     * @param x2 int next x coordinate
     * @param y2 int next y coordinate
     * @param color int representing a colour in {@link android.graphics.Color}.
     */
    @Override
    public void drawLine(int x, int y, int x2, int y2, int color) {
        paint.setColor(color);
        canvas.drawLine(x, y, x2, y2, paint);
    }

    /**
     * Draws a rectangle based upon a single coordinate, of a specified height and width and colour.
     * @param x int starting x coordinate of rectangle
     * @param y int starting y coordinate of rectangle
     * @param width int width of rectangle
     * @param height int height of rectangle
     * @param color int representing a colour in {@link android.graphics.Color}.
     */
    @Override
    public void drawRect(int x, int y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(x, y, x + width - 1, y + height - 1, paint);
    }

    /**
     *
     * @param a
     * @param r
     * @param g
     * @param b
     */
    @Override
    public void drawARGB(int a, int r, int g, int b) {
        paint.setStyle(Style.FILL);
       canvas.drawARGB(a, r, g, b);
    }

    /**
     * Draws a string on the canvas from a coordinate and a specified string properties such as
     * bold or underlining etc.
     * @param text String text to draw.
     * @param x int x coordinate of text to be drawn
     * @param y int y coordinate of text to be drawn
     * @param paint Paint containing format of string, underlining, bold, italics etc.
     * @see Paint
     */
    @Override
    public void drawString(String text, int x, int y, Paint paint){
        canvas.drawText(text, x, y, paint);

        
    }
    

    public void drawImage(Image Image, int x, int y, int srcX, int srcY,
            int srcWidth, int srcHeight) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + srcWidth;
        dstRect.bottom = y + srcHeight;

        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect,
                null);
    }

    /**
     * Draws an image on the canvas.
     * @param Image Image loaded image that will be displayed on the canvas.
     * @param x int x coordinate of image to be drawn
     * @param y int y coordinate of image to be drawn
     * @see Image
     * @see AndroidImage
     */
    @Override
    public void drawImage(Image Image, int x, int y) {
        canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y, null);
    }

    /**
     * Draws an image on the canvas whilst scaling it correctly.
     * @param Image Image loaded image that will be displayed on the canvas.
     * @param x int x coordinate of image to be drawn
     * @param y int y coordinate of image to be drawn
     * @param width
     * @param height
     * @param srcX
     * @param srcY
     * @param srcWidth Int width of the image
     * @param srcHeight Int height of the image
     */
    public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight){
        
        
     srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + width;
        dstRect.bottom = y + height;
        
   
        
        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect, null);
        
    }

    /**
     * Gets the width of the framebuffer coordinate system. e.g. returning 480 of a 480 x 800 buffer.
     * @return int width of the buffer.
     */
    @Override
    public int getWidth() {
        return frameBuffer.getWidth();
    }
    /**
     * Gets the height of the framebuffer coordinate system. e.g. returning 800 of a 480 x 800 buffer.
     * @return int height of the buffer.
     */
    @Override
    public int getHeight() {
        return frameBuffer.getHeight();
    }
}
