package uk.ac.uea.framework.implementation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

import uk.ac.uea.framework.Audio;
import uk.ac.uea.framework.FileIO;
import uk.ac.uea.framework.Game;
import uk.ac.uea.framework.Graphics;
import uk.ac.uea.framework.Input;
import uk.ac.uea.framework.Screen;
/**
 * Manages the entire AndroidGame. Implements Interface Game and Extends Activity.
 * Contains a AndroidFastRenderView, Graphics, Audio, Input, FileIO, Screen and WakeLock.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see Activity
 * @see Game
 * @see AndroidFastRenderView
 * @see Graphics
 * @see Audio
 * @see Input
 * @see FileIO
 * @see Screen
 * @see WakeLock
 */
public abstract class AndroidGame extends Activity implements Game {
    AndroidFastRenderView renderView;
    Graphics graphics;
    Audio audio;
    Input input;
    FileIO fileIO;
    Screen screen;
    WakeLock wakeLock;

    /**
     * On creation of the application it sets up the windows, screen dimensions and aspect ratios.
     * Instantiating the AndroidFastRenderView, AndroidGraphics, AndroidFileIO, AndroidAudio,
     * AndroidInput and Screen.
     * @param savedInstanceState
     * @see AndroidFastRenderView
     * @see AndroidGraphics
     * @see AndroidFileIO
     * @see AndroidAudio
     * @see AndroidInput
     * @see Screen
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        int frameBufferWidth = isPortrait ? 480: 800;
        int frameBufferHeight = isPortrait ? 800: 480;
        Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
                frameBufferHeight, Config.RGB_565);
        
        float scaleX = (float) frameBufferWidth
                / getWindowManager().getDefaultDisplay().getWidth();
        float scaleY = (float) frameBufferHeight
                / getWindowManager().getDefaultDisplay().getHeight();

        renderView = new AndroidFastRenderView(this, frameBuffer);
        graphics = new AndroidGraphics(getAssets(), frameBuffer);
        fileIO = null;//new AndroidFileIO(this);
        audio = new AndroidAudio(this);
        input = new AndroidInput(this, renderView, scaleX, scaleY);
        screen = getInitScreen();
        setContentView(renderView);
        
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "MyGame");
    }

    /**
     * Method handles the reaction after resuming from a paused state. Requiring the wakelock and
     * resuming the Screen. Continues rendering.
     * @see AndroidFastRenderView
     * @see Screen
     * @see WakeLock
     */
    @Override
    public void onResume() {
        super.onResume();
        wakeLock.acquire();
        screen.resume();
        renderView.resume();
    }

    /**
     * Method handles the reaction on pausing of the screen. Releasing the wakelock and
     * pausing the Screen. Disposes of screen whilst pausing the rendering.
     * @see AndroidFastRenderView
     * @see Screen
     * @see WakeLock
     */
    @Override
    public void onPause() {
        super.onPause();
        wakeLock.release();
        renderView.pause();
        screen.pause();

        if (isFinishing())
            screen.dispose();
    }

    @Override
    public Input getInput() {
        return input;
    }

    @Override
    public FileIO getFileIO() {
        return fileIO;
    }

    @Override
    public Graphics getGraphics() {
        return graphics;
    }

    @Override
    public Audio getAudio() {
        return audio;
    }

    /**
     * Method to set a parsed screen to the current screen.
     * Pauses the current screen and disposes of it. Resumes the parsed screen, updating it and
     * setting it as the new screen.
     * @param screen Screen parsed to be set.
     * @see Screen
     */
    @Override
    public void setScreen(Screen screen) {
        if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        this.screen.dispose();
        screen.resume();
        screen.update(0);
        this.screen = screen;
    }
    
    public Screen getCurrentScreen() {

        return screen;
    }
}
