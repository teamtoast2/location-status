package uk.ac.uea.framework;

/**
 * Abstract class, handles Screen management. Provides overidble methods for funtionality of a Screen.
 * No direct Android implementation.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 */
public abstract class Screen {
    protected final Game game;

    public Screen(Game game) {
        this.game = game;
    }

    public abstract void update(float deltaTime);

    public abstract void paint(float deltaTime);

    public abstract void pause();

    public abstract void resume();

    public abstract void dispose();
    
    public abstract void backButton();
}
