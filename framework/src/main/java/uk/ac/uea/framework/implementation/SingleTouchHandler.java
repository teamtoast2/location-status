package uk.ac.uea.framework.implementation;

import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;
import android.view.View;

import uk.ac.uea.framework.Pool;
import uk.ac.uea.framework.Input.TouchEvent;
import uk.ac.uea.framework.Pool.PoolObjectFactory;
/**
 * Handles input of touch events on a single touch screen. Implements TouchHandler.
 * Contains member variables of the x and y coordinate of touch on screen, scale of X and Y on
 * screen with coordinate system. A Pool of TouchEvents, List of TouchEvents and Buffer List of Touch Events.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see TouchHandler
 */
public class SingleTouchHandler implements TouchHandler {
    boolean isTouched;
    int touchX;
    int touchY;
    Pool<TouchEvent> touchEventPool;
    List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();
    List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();
    float scaleX;
    float scaleY;

    /**
     * Creates SingleTouchHandler. Initialises a PoolObjectFactory of TouchEvents and assigns that
     * to the TouchEvent pool, specifying max size of 100 TouchEvents.
     * @param view View, used to handle the rendering of the view.
     * @param scaleX Float, ratio of x axis coordinate system and screen width, assigned to member variable.
     * @param scaleY Float, ratio of y axis coordinate system and screen height, assigned to member variable.
     * @see TouchEvent
     * @see View
     * @see PoolObjectFactory
     * @see Pool
     */
    public SingleTouchHandler(View view, float scaleX, float scaleY) {
        PoolObjectFactory<TouchEvent> factory = new PoolObjectFactory<TouchEvent>() {
            @Override
            public TouchEvent createObject() {
                return new TouchEvent();
            }            
        };
        touchEventPool = new Pool<TouchEvent>(factory, 100);
        view.setOnTouchListener(this);

        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    /**
     * Handles what TouchEvent type has occurred and its respective reaction. Switches on TouchEvent.type.
     * Also registers the coordinates of the TouchEvent.
     * @param v View, used to handle the rendering of the view.
     * @param event, MotionEvent
     * @return Boolean True if remains touching after event.
     * @see TouchEvent
     * @see MotionEvent
     * @see View
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        synchronized(this) {
            TouchEvent touchEvent = touchEventPool.newObject();
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchEvent.type = TouchEvent.TOUCH_DOWN;
                isTouched = true;
                break;
            case MotionEvent.ACTION_MOVE:
                touchEvent.type = TouchEvent.TOUCH_DRAGGED;
                isTouched = true;
                break;
            case MotionEvent.ACTION_CANCEL:                
            case MotionEvent.ACTION_UP:
                touchEvent.type = TouchEvent.TOUCH_UP;
                isTouched = false;
                break;
            }
            
            touchEvent.x = touchX = (int)(event.getX() * scaleX);
            touchEvent.y = touchY = (int)(event.getY() * scaleY);
            touchEventsBuffer.add(touchEvent);                        
            
            return true;
        }
    }

    /**
     * Checks if this handler has registered its current event as touched.
     * Overridden Interface method, no true use.
     * @param pointer int representing id into TouchEvent pool.
     * @return Boolean if Touched True
     */
    @Override
    public boolean isTouchDown(int pointer) {
        synchronized(this) {
            if(pointer == 0)
                return isTouched;
            else
                return false;
        }
    }

    /**
     * Gets the x coordinate of the current registered touch.
     * @param pointer int representing id into TouchEvent pool.
     * @return int x coordinate.
     */
    @Override
    public int getTouchX(int pointer) {
        synchronized(this) {
            return touchX;
        }
    }
    /**
     * Gets the y coordinate of the current registered touch.
     * @param pointer int representing id into TouchEvent pool.
     * @return int y coordinate.
     */
    @Override
    public int getTouchY(int pointer) {
        synchronized(this) {
            return touchY;
        }
    }

    /**
     * Gets the List of TouchEvents that has been recorded by the TouchHandler.
     * @return List<TouchEvent> all recorded TouchEvents from this handler.
     * @see TouchHandler
     * @see TouchEvent
     */
    @Override
    public List<TouchEvent> getTouchEvents() {
        synchronized(this) {     
            int len = touchEvents.size();
            for( int i = 0; i < len; i++ )
                touchEventPool.free(touchEvents.get(i));
            touchEvents.clear();
            touchEvents.addAll(touchEventsBuffer);
            touchEventsBuffer.clear();
            return touchEvents;
        }
    }
}
