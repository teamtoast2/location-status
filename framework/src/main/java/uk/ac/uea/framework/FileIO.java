package uk.ac.uea.framework;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.content.SharedPreferences;
/**
 * Interface, handles the reading and writing of files. See implementation {@link uk.ac.uea.framework.implementation.AndroidFileIO}
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * */
public interface FileIO {
    public InputStreamReader readFile(String file) throws IOException;

    public void writeFile(String file, String content) throws IOException;
    
   // public InputStream readAsset(String file) throws IOException;
    
   // public SharedPreferences getSharedPref();
}
