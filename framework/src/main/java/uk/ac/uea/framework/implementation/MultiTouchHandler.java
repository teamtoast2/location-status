package uk.ac.uea.framework.implementation;

import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;
import android.view.View;

import uk.ac.uea.framework.Pool;
import uk.ac.uea.framework.Input.TouchEvent;
import uk.ac.uea.framework.Pool.PoolObjectFactory;

/**
 * Handles input of touch events on a multi touch screen. Implements TouchHandler.
 * Contains member arrays of the x and y coordinate of touchs on screen, scale of X and Y on
 * screen with coordinate system. A Pool of TouchEvents, List of TouchEvents and Buffer List of Touch Events.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see TouchHandler
 */
public class MultiTouchHandler implements TouchHandler {
    private static final int MAX_TOUCHPOINTS = 10;
    
    boolean[] isTouched = new boolean[MAX_TOUCHPOINTS];
    int[] touchX = new int[MAX_TOUCHPOINTS];
    int[] touchY = new int[MAX_TOUCHPOINTS];
    int[] id = new int[MAX_TOUCHPOINTS];
    Pool<TouchEvent> touchEventPool;
    List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();
    List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();
    float scaleX;
    float scaleY;


    /**
     * Creates a MultiTouchHandler. Initialises a PoolObjectFactory of TouchEvents and assigns that
     * to the TouchEvent pool, specifying max size of 100 TouchEvents.
     * @param view View, used to handle the rendering of the view.
     * @param scaleX Float, ratio of x axis coordinate system and screen width, assigned to member variable.
     * @param scaleY Float, ratio of y axis coordinate system and screen height, assigned to member variable.
     * @see TouchEvent
     * @see View
     * @see PoolObjectFactory
     * @see Pool
     */
    public MultiTouchHandler(View view, float scaleX, float scaleY) {
        PoolObjectFactory<TouchEvent> factory = new PoolObjectFactory<TouchEvent>() {
            @Override
            public TouchEvent createObject() {
                return new TouchEvent();
            }
        };
        touchEventPool = new Pool<TouchEvent>(factory, 100);
        view.setOnTouchListener(this);

        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }
    /**
     * Handles what TouchEvent type has occurred and its respective reaction. Switches on MotionEvent.action.
     * Also registers the coordinates of the TouchEvent.
     * @param v View, used to handle the rendering of the view.
     * @param event, MotionEvent
     * @return Boolean True if remains touching after event.
     * @see TouchEvent
     * @see MotionEvent
     * @see View
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        synchronized (this) {
            int action = event.getAction() & MotionEvent.ACTION_MASK;
            int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
            int pointerCount = event.getPointerCount();
            TouchEvent touchEvent;
            for (int i = 0; i < MAX_TOUCHPOINTS; i++) {
                if (i >= pointerCount) {
                    isTouched[i] = false;
                    id[i] = -1;
                    continue;
                }
                int pointerId = event.getPointerId(i);
                if (event.getAction() != MotionEvent.ACTION_MOVE && i != pointerIndex) {
                    // if it's an up/down/cancel/out event, mask the id to see if we should process it for this touch
                    // point
                    continue;
                }
                switch (action) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    touchEvent = touchEventPool.newObject();
                    touchEvent.type = TouchEvent.TOUCH_DOWN;
                    touchEvent.pointer = pointerId;
                    touchEvent.x = touchX[i] = (int) (event.getX(i) * scaleX);
                    touchEvent.y = touchY[i] = (int) (event.getY(i) * scaleY);
                    isTouched[i] = true;
                    id[i] = pointerId;
                    touchEventsBuffer.add(touchEvent);
                    break;

                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                case MotionEvent.ACTION_CANCEL:
                    touchEvent = touchEventPool.newObject();
                    touchEvent.type = TouchEvent.TOUCH_UP;
                    touchEvent.pointer = pointerId;
                    touchEvent.x = touchX[i] = (int) (event.getX(i) * scaleX);
                    touchEvent.y = touchY[i] = (int) (event.getY(i) * scaleY);
                    isTouched[i] = false;
                    id[i] = -1;
                    touchEventsBuffer.add(touchEvent);
                    break;

                case MotionEvent.ACTION_MOVE:
                    touchEvent = touchEventPool.newObject();
                    touchEvent.type = TouchEvent.TOUCH_DRAGGED;
                    touchEvent.pointer = pointerId;
                    touchEvent.x = touchX[i] = (int) (event.getX(i) * scaleX);
                    touchEvent.y = touchY[i] = (int) (event.getY(i) * scaleY);
                    isTouched[i] = true;
                    id[i] = pointerId;
                    touchEventsBuffer.add(touchEvent);
                    break;
                }
            }
            return true;
        }
    }
    /**
     * Checks if this handler has registered its current event as touched.
     * @param pointer int representing id into TouchEvent pool. Checks if this TouchEvent referenced
     *                by pointer is classified as touched.
     * @return Boolean if Touched True
     */
    @Override
    public boolean isTouchDown(int pointer) {
        synchronized (this) {
            int index = getIndex(pointer);
            if (index < 0 || index >= MAX_TOUCHPOINTS)
                return false;
            else
                return isTouched[index];
        }
    }
    /**
     * Gets the x coordinate of the current registered touch. Default 0.
     * @param pointer int representing id into TouchEvent pool.
     * @return int x coordinate.
     */
    @Override
    public int getTouchX(int pointer) {
        synchronized (this) {
            int index = getIndex(pointer);
            if (index < 0 || index >= MAX_TOUCHPOINTS)
                return 0;
            else
                return touchX[index];
        }
    }
    /**
     * Gets the y coordinate of the current registered touch. Default 0.
     * @param pointer int representing id into TouchEvent pool.
     * @return int y coordinate.
     */
    @Override
    public int getTouchY(int pointer) {
        synchronized (this) {
            int index = getIndex(pointer);
            if (index < 0 || index >= MAX_TOUCHPOINTS)
                return 0;
            else
                return touchY[index];
        }
    }
    /**
     * Gets the List of TouchEvents that has been recorded by the TouchHandler.
     * @return List<TouchEvent> all recorded TouchEvents from this handler.
     * @see TouchHandler
     * @see TouchEvent
     */
    @Override
    public List<TouchEvent> getTouchEvents() {
        synchronized (this) {
            int len = touchEvents.size();
            for (int i = 0; i < len; i++)
                touchEventPool.free(touchEvents.get(i));
            touchEvents.clear();
            touchEvents.addAll(touchEventsBuffer);
            touchEventsBuffer.clear();
            return touchEvents;
        }
    }

    /**
     * Returns the index for a given pointerId or -1 if no index.
     * @param pointerId int representing id into TouchEvent pool.
     * @return int index of point in id array.
     */
    private int getIndex(int pointerId) {
        for (int i = 0; i < MAX_TOUCHPOINTS; i++) {
            if (id[i] == pointerId) {
                return i;
            }
        }
        return -1;
    }
}
