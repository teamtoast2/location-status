package uk.ac.uea.framework.implementation;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import uk.ac.uea.framework.Audio;
import uk.ac.uea.framework.Music;
import uk.ac.uea.framework.Sound;

/**
 * Handles the creation of Audio, in the form of Music or Sound. Implements Interface Audio.
 * Contains an Asset Manager and Sound Pool.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see AssetManager
 * @see SoundPool
 * @see Audio
 */
public class AndroidAudio implements Audio {
    AssetManager assets;
    SoundPool soundPool;

    /**
     * Creates AndroidAudio Object.
     * @param activity Activity parsed to set the volume control stream, extract the assets
     *                 contained in the Activity and initialise a Sound Pool object.
     * @see Activity
     * @see AssetManager
     */
    public AndroidAudio(Activity activity) {
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
    }

    /**
     * Creates a Music Object by loading a file into an AssetFileDescriptor.
     * @param filename String filepath of the Music file.
     * @return AndroidMusic sub class Object of Music.
     * @see Music
     * @see AssetFileDescriptor
     * @see AndroidMusic
     */
    @Override
    public Music createMusic(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            return new AndroidMusic(assetDescriptor);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load music '" + filename + "'");
        }
    }

    /**
     * Creates a Sound Object by loading a file into an AssetFileDescriptor.
     * @param filename String filepath of the Sound file to extract from SoundPool.
     * @return AndroidSound sub class Object of Sound.
     * @see Sound
     * @see AssetFileDescriptor
     * @see SoundPool
     * @see AndroidSound
     */
    @Override
    public Sound createSound(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            int soundId = soundPool.load(assetDescriptor, 0);
            return new AndroidSound(soundPool, soundId);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load sound '" + filename + "'");
        }
    }
}
