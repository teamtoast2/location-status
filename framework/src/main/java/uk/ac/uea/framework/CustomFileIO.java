package uk.ac.uea.framework;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


/**
 * Created by qrk13cdu on 15/12/2015.
 */
public class CustomFileIO extends Activity{

    public String readFile(Activity activity){
        String s="";
        try {
            FileInputStream fileIn=activity.getBaseContext().openFileInput("post.hist");
            InputStreamReader InputRead= new InputStreamReader(fileIn);
            char[] inputBuffer= new char[1000];
            int charRead;

            while ((charRead=InputRead.read(inputBuffer))>0) {
                String readstring=String.copyValueOf(inputBuffer,0,charRead);
                s +=readstring;
            }
            InputRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    public void writeFile(Activity activity, String content) {
        try {
            FileOutputStream fileout=activity.getBaseContext().openFileOutput("post.hist", MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
            outputWriter.write(content);
            outputWriter.close();
        } catch (IOException e) {
            System.out.println(e);
        }

    }
}
