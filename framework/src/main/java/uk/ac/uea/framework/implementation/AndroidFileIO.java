package uk.ac.uea.framework.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;

import uk.ac.uea.framework.FileIO;

/**
 * Handles the reading and writing of files. Implements FileIO.
 * Contains a Context and an AssetManager and member variable external storage path location.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see Context
 * @see AssetManager
 * @see FileIO
 */
public class AndroidFileIO {

    Context context;
    AssetManager assets;
    String externalStoragePath;

    /**
     * Creates an AndroidAudio Object. Handles the location of the external storage within using
     * Environment class and stored as member variable.
     * @param context Context used to get the assets from within and store as the AssetManager.
     *                Context also stored within this class.
     * @see Context
     * @see AssetManager
     * @see Environment
     */
    public AndroidFileIO(Context context) {
        this.context = context;
        this.assets = context.getAssets();
        this.externalStoragePath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + File.separator;
        
 
    
    }

    /**
     * Handles the reading of an asset.
     * @param file String location of the asset to load.
     * @return InputStream of opened assets contents.
     * @throws IOException
     */

    public InputStream readAsset(String file) throws IOException {
        return assets.open(file);
    }

    /**
     * Handles the reading of a file.
     * @param file String location of the file to load, concatenated with external storage path
     *             created in constructor.
     * @return InputStream of opened files contents.
     * @throws IOException
     */

    public InputStream readFile(String file) throws IOException {
        return new FileInputStream(externalStoragePath + file);
    }

    /**
     * Handles the writing of a file.
     * @param file String destination location, concatenated with external storage path member variable.
     * @return OutputStream of writing files contents.
     * @throws IOException
     */

    public OutputStream writeFile(String file) throws IOException {
        return new FileOutputStream(externalStoragePath + file);
    }

    /**
     * Retrieves the shared preferences of the member Context using a Preference Manager.
     * @return SharedPreferences
     * @see PreferenceManager
     * @see Context
     */
    public SharedPreferences getSharedPref() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
