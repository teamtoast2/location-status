package uk.ac.uea.framework;

/**
 * Created by qrk13cdu on 17/11/2015.
 */
public abstract class Location {
    protected float currentLattitude;
    protected float currentLongitude;

    public Location(float currentLattitude, float currentLongitude) {
        this.currentLattitude = currentLattitude;
        this.currentLongitude = currentLongitude;
    }

    public abstract void aquireLocation();

    /*
    Getter and setter methods.
     */
    public float getCurrentLattitude() {
        return currentLattitude;
    }

    public void setCurrentLattitude(float currentLattitude) {
        this.currentLattitude = currentLattitude;
    }

    public float getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(float currentLongitude) {
        this.currentLongitude = currentLongitude;
    }
}
