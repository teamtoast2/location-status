package uk.ac.uea.framework.implementation;

import android.graphics.Bitmap;

import uk.ac.uea.framework.Image;
import uk.ac.uea.framework.Graphics.ImageFormat;
/**
 * Handles Images, implementing Image.
 * Contains a Bitmap of representing the image and an Image format representing either
 * ARGB8888, ARGB4444, RGB565.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see Image
 */
public class AndroidImage implements Image {
    Bitmap bitmap;
    ImageFormat format;

    /**
     * Creates an AndroidImage.
     * @param bitmap Bitmap representing Image
     * @param format ImageFormat format of the Image
     * @see ImageFormat
     */
    public AndroidImage(Bitmap bitmap, ImageFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }

    @Override
    public int getWidth() {
        return bitmap.getWidth();
    }

    @Override
    public int getHeight() {
        return bitmap.getHeight();
    }

    @Override
    public ImageFormat getFormat() {
        return format;
    }

    /**
     * Discards the current image stored.
     * @see Bitmap
     */
    @Override
    public void dispose() {
        bitmap.recycle();
    }      
}
