package uk.ac.uea.framework;
/**
 * Interface, proving structure to manages the AndroidGame. See implementation {@link uk.ac.uea.framework.implementation.AndroidGame}
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 */
public interface Game {

	public Audio getAudio();

    public Input getInput();

    public FileIO getFileIO();

    public Graphics getGraphics();

    public void setScreen(Screen screen);

    public Screen getCurrentScreen();

    public Screen getInitScreen();
	
}
