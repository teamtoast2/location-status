DROP TABLE post;

CREATE TABLE post
(
postId SERIAL PRIMARY KEY,
content VARCHAR(1000),
zone VARCHAR(1000), 
likes INT,
dateCreated TIMESTAMP,
lat FLOAT,
lng FLOAT
);

INSERT INTO post 
VALUES('-1','Threw up at JSC LOL', 'Julian Study Centre',0,TO_TIMESTAMP('1991/07/06 08:18:15', 'YYYY-MM-DD HH24:MI:SS') ,'50.988801', '-1.498875');
INSERT INTO post 
VALUES('-2','Fell over in thomas paine :\', 'Thomas Paine',0,TO_TIMESTAMP('1991/07/06 08:18:15', 'YYYY-MM-DD HH24:MI:SS') ,'52.621926', '1.236140');
INSERT INTO post 
VALUES('-3','Ventured a little too far out.. HELP', 'Uncategorised location',0,TO_TIMESTAMP('1991/07/06 08:18:15', 'YYYY-MM-DD HH24:MI:SS') ,'52.679408', '1.148445');