package uk.ac.uea.locstat.Models;

import com.team_toast.framework.AppManagement.FrameworkCopyright;

/**
 * Created by qrk13cdu on 14/10/2015.
 */
public class AppCopyright extends FrameworkCopyright {

    public String getAppCopyright(){
        return "This application is created by Team Toast (c) 2015";
    }
}
