package uk.ac.uea.locstat;
import android.app.ListActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.team_toast.framework.AppManagement.Notification;
import com.team_toast.framework.AppManagement.Permissions;
import com.team_toast.framework.Database.DatabaseAccessor;
import com.team_toast.framework.Location.Location;
import com.team_toast.framework.Location.AndroidGpsLocation;
import com.team_toast.framework.StateManager.Caretaker;
import com.team_toast.framework.StateManager.StringOriginator;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import android.os.Handler;
import uk.ac.uea.locstat.Controllers.PostController;
import uk.ac.uea.locstat.Controllers.PreviousPostController;
import uk.ac.uea.locstat.Controllers.ZoneController;
import uk.ac.uea.locstat.Models.Post;
import uk.ac.uea.locstat.Models.User;
import uk.ac.uea.locstat.Models.Zone;

/**
 * Main page of the application. Handles the fields to create a post, the displaying of posts,
 * undo button and previous post redirection.
 *
 * Framework usage
 * -Memento undo button
 * -Notifcations
 */
public class MainActivity extends ListActivity {

    static public User user;
    public final static String EXTRA_MESSAGE = "uk.ac.uea.locstat.MESSAGE";
    public ArrayList<Post> postsToDisplay;
    Handler handler;
    ZoneController zoneHandler;
    boolean locationFound = true;
    //Memento Vars
    public Caretaker careTaker;
    public StringOriginator originator;

    @Override
    public void onResume() {  // After a pause OR at startup

        super.onResume();
            //stars the update zone checks

            try {
                //load public posts from database, based upon the current users zone/location
                postsToDisplay = PostController.createArrayListOfPosts(new DatabaseAccessor().execute("select * from post where zone ='" + user.getZone().getName() + "'order by datecreated desc").get());
            } catch (Exception e) {
                System.out.println(e);
            }
            //display the fetched posts from the database
            this.displayAsListView(postsToDisplay);

            //check to see which class this class is as PreviousPosts inherits from this to save reuse.
            if (this.getClass() == MainActivity.class) {

                //updates the character counter on the post creation page
                EditText et = (EditText) findViewById(R.id.editText);
                et.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void afterTextChanged(Editable s) {
                        TextView textView = (TextView) findViewById(R.id.textView4);
                        textView.setText(String.valueOf(150 - s.toString().length()));
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int st, int b, int c) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int st, int c, int a) {
                    }
                });
            }
        }


    /**
     * Update zone actively checks to see if the user has entered a new zone in its own runnable
     * thread.
     * @param mainActivity
     */
    public void updateZone(final MainActivity mainActivity){
        final Runnable updateTask=new Runnable() {
            @Override
            public void run() {
                Zone prevZone = user.getZone();
                Location appLocation = new AndroidGpsLocation(mainActivity);
                appLocation.initialiseLocationManager(mainActivity);
                appLocation.updateToLastKnownLocation();

                    user.setCurrentLocation(appLocation);
                    user.setZone(zoneHandler.getZoneByLocation(user.getCurrentLocation()));
                    if (user.getZone().getName() != prevZone.getName()) {

                        //if the zone has changed, send the user a notification
                        new Notification().pushNotification(mainActivity, R.drawable.common_signin_btn_icon_dark,
                                "You have entered a new Zone!", "You are now in " + user.getZone().getName() + "", new Intent(mainActivity, MainActivity.class));

                    }
                    //constantly update the users zone on the main page
                    TextView tv = (TextView) findViewById(R.id.textView);
                    tv.setText(user.getZone().getName());
                    //on resume to fetch any new posts and display them
                    onResume();

                //continuous self call
                handler.postDelayed(this, 1000);
            }
        };
        //initial thread call
        handler.postDelayed(updateTask, 1000);
    }

    /**
     * Create Post, sets up the creation of a post by checking fields for correct input here and
     * in the manifest by checking for appropriate characters. Also handles memento enteries for the
     * undo button on the home page.
     * @param view
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws JSONException
     */
    public void createPost(View view) throws InterruptedException, ExecutionException, JSONException {

        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        //checks if the field has any characters in other than whitespace.
        if(editText.getText().toString().trim().length() > 0){
            PostController.createPost(this);

            //Save state of String
            originator.setMyValue(message);
            careTaker.addMemento(originator.saveToMemento());

            onResume();
        }else{
            //clears any white space post and resets it to display the hint to enter a message
            editText.setText("");
        }
    }

    /**
     * Executes when the undo button is clicked
     * @param view  the view of the activity
     */
    public void previousPost(View view) {

        //get previous state
        originator.restoreFromMemento(careTaker.getMemento());
        String message = originator.getMyValue();

        //apply text to search bar
        EditText editText = (EditText) findViewById(R.id.editText);
        editText.setText(message);
    }
    /**
     * Creates an intent to go back to the previous posts page.
     */
    public void gotoPreviousPosts(View view) {
        Intent i = new Intent(this, PreviousPostController.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Permissions permissions = new Permissions();
        permissions.requestFinePermissions(this);
        permissions.requestCoarsePermissions(this);
        //Initialise Memento vars
        careTaker = new Caretaker();
        originator = new StringOriginator();

        zoneHandler = new ZoneController(this);

        //Checks if this is the first initialisation of the app
        if (savedInstanceState == null && this.getClass() == MainActivity.class) {
            //initialises all one instance only varaibles
            user = new User();
            handler = new Handler();
            this.updateZone(this);
        }
        setContentView(R.layout.activity_main);
    }

    /**
     * Creates and displays a list view dynamically, each entry in the arraylist given
     * is a clickable entry in the list view.
     * @param postsToDisplay
     */
    public void displayAsListView(final ArrayList<Post> postsToDisplay) {
        ListView listView1 = (ListView) findViewById(android.R.id.list);
        ArrayAdapter<Post> adapter = new ArrayAdapter<Post>(getListView().getContext(), android.R.layout.simple_list_item_1, postsToDisplay);
        getListView().setAdapter(adapter);
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                //on click it sends the position of the entry it click to allow me to identify
                //which post is clicked on
                //also sends the list of posts to allow the position to index into to identify
                //the post.
                sendPost(view, position, postsToDisplay);
            }
        });
    }

    /**
     * Send post, handles when a post is clicked on in the dynamic list view.
     * @param view
     * @param i
     * @param posts
     */
    public void sendPost(View view, int i, ArrayList<Post> posts) {
        //gives the controller access to the list of posts
        PostController.posts=posts;
        Intent intent = new Intent(this, PostController.class);
        // puts the positional index into the intent to then give to the post controller
        intent.putExtra(EXTRA_MESSAGE, "" + i);
        startActivity(intent);
    }
}

