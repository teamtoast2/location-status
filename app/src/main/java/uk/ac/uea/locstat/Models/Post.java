package uk.ac.uea.locstat.Models;
import com.team_toast.framework.Location.Location;
import java.sql.Timestamp;

/**
 * Created by qrk13cdu on 17/11/2015.
 */
public class Post{
    private int postId;
    private String content;
    private Location location;
    private Zone zone;
    private int likes;
    private Timestamp timeStamp;

    public Post(int postId, String content, Location location, Zone zone, int likes, Timestamp timeStamp) {
        this.postId = postId;
        this.content = content;
        this.location = location;
        this.zone = zone;
        this.likes = likes;
        this.timeStamp=timeStamp;
    }

    public Post(int postId, String content, Zone zone, int likes, Timestamp timeStamp) {
        this.postId = postId;
        this.content = content;
        this.location = location;
        this.zone = zone;
        this.likes = likes;
        this.timeStamp=timeStamp;
    }

    public Post(String content, Location location, Zone zone, Timestamp timeStamp){
        this.postId=-1;
        this.content = content;
        this.location = location;
        this.zone = zone;
        this.likes = 0;
        this.timeStamp=timeStamp;
    }

    public int upVotePost(){
        return ++this.likes;
    }

    public int downVotePost(){
        return --this.likes;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public int getLikes() {
        return likes;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return content;
    }
}
