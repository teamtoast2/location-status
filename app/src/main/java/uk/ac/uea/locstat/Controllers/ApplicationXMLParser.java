package uk.ac.uea.locstat.Controllers;

import android.content.Context;

import com.team_toast.framework.DataInterchange.AndroidXmlParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import uk.ac.uea.locstat.Models.Zone;

/**
 * Extension of the framework AndroidXmlParser to allow app specific objects to be created.
 * Framework usage
 * -AndroidXMLParser
 */
public class ApplicationXMLParser extends AndroidXmlParser {

    public ApplicationXMLParser(Context appContext){
        super(appContext);
    }

    /**
     * Creates a lsit of zones from the frameworks parser.
     * @throws XmlPullParserException
     * @throws IOException
     */
    @Override
    public void parseData() throws XmlPullParserException, IOException {
        ArrayList<Zone> tempZones = null;
        int eventType = parser.getEventType();
        Zone zone = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name = null;
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    tempZones = new ArrayList();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equals("zone")) {
                        zone = new Zone();
                    } else if (zone != null) {
                        if (name.equals("name")) {
                            zone.setName(parser.nextText());
                        } else if (name.equals("latitude")) {
                            zone.setLatitude(Float.parseFloat(parser.nextText()));
                        } else if (name.equals("longitude")) {
                            zone.setLongitude(Float.parseFloat(parser.nextText()));
                        } else if (name.equals("radius")) {
                            zone.setRadius(Float.parseFloat(parser.nextText()));
                        }
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (zone != null) {
                        tempZones.add(zone);
                        zone = null;
                    }
            }
            eventType = parser.next();
        }
        ZoneController.zones = tempZones;
    }


}
