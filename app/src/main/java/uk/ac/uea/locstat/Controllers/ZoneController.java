package uk.ac.uea.locstat.Controllers;

import android.content.Context;
import com.team_toast.framework.Location.Location;
import java.util.ArrayList;
import uk.ac.uea.locstat.Models.Pair;
import uk.ac.uea.locstat.Models.Zone;

/**
 * Handles the managment of Zones, checks if new zones have been entered by performing distance
 * calculations.
 *
 * Framework usage
 * -XML pull parser which is an extension of the framework parser
 */
public class ZoneController {
    public static ArrayList<Zone> zones;

    public ZoneController(Context context){
        ApplicationXMLParser parser = new ApplicationXMLParser(context);
        try {
            parser.loadFile("zones.xml");
        }catch(Exception e){
            System.out.println(e);
        }

    }
    public Zone getZoneByLocation(Location location){
        float latA = (float)location.getLatLng().latitude;
        float longA = (float)location.getLatLng().longitude;

        float masterLat = 52.623165f;
        float masterLong = 1.238721f;
        float masterRadius = 900f;

        if (distFrom(latA, longA, masterLat, masterLong) > masterRadius) {
            //Device is outside of the university
            return new Zone("Uncategorised location");
        }

        ArrayList<Pair> possibleZones = new ArrayList<>();
        Zone zone = null;
        float latB;
        float longB;
        float radius;

        float d; //Distance
        float smallestDistance = Float.MAX_VALUE;
        int closestIndex = 0;

        for (int i = 0; i < zones.size(); i++) {

            //Get lat and long from location in zones
            latB = zones.get(i).getLatitude();
            longB = zones.get(i).getLongitude();
            radius = zones.get(i).getRadius();

            //Get distance between 2 positions
            d = distFrom(latA, longA, latB, longB);

            //Collision
            if (d < radius)
                possibleZones.add(new Pair(d, zones.get(i)));
            else {
                //Keep track of index with closest distance incase no matches found
                if (d < smallestDistance) {
                    smallestDistance = d;
                    closestIndex = i;
                }
            }
        }

        //If no matches were found, return closest zone
        if (possibleZones.size() <= 0) {
            zone = zones.get(closestIndex);
        }

        //If only one possible zone, use that
        else if (possibleZones.size() == 1) {
            zone = possibleZones.get(0).getZone();
        }

        //Find the closest zone in the possible zones
        else {

            float distance = Float.MAX_VALUE;

            for (int i = 0; i < possibleZones.size(); i++) {
                if (possibleZones.get(i).getDistance() < distance) {
                    distance = possibleZones.get(i).getDistance();
                    zone = possibleZones.get(i).getZone();
                }
            }
        }

        //Return zone
        return zone;
    }


    public static float distFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }
}
