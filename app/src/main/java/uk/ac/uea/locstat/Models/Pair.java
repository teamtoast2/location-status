package uk.ac.uea.locstat.Models;

/**
 * Created by James on 15/12/2015.
 */
public class Pair {
    private float distance;
    private Zone zone;

   public Pair(float distance, Zone zone) {
        this.distance = distance;
        this.zone = zone;
    }

    public float getDistance() { return distance; }
    public Zone getZone() { return zone; }
}
