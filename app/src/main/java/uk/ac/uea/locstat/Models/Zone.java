package uk.ac.uea.locstat.Models;

/**
 * Created by James on 15/12/2015.
 */
public class Zone {

    private String name;
    private float latitude;
    private float longitude;
    private float radius;

    public Zone() {}
    public Zone(String name){
        this.name=name;
    }

    public String getName() { return name; }
    public float getLatitude() { return latitude; }
    public float getLongitude() { return longitude; }
    public float getRadius() { return radius; }

    public void setName(String name) { this.name = name; }
    public void setLatitude(float latitude) { this.latitude = latitude; }
    public void setLongitude(float longitude) { this.longitude = longitude; }
    public void setRadius(float radius) { this.radius = radius; }
}
