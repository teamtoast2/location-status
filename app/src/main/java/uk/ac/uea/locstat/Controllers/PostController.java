package uk.ac.uea.locstat.Controllers;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.team_toast.framework.Maps.AndroidMap;
import com.team_toast.framework.Database.DatabaseAccessor;
import com.team_toast.framework.Location.Location;
import com.team_toast.framework.Location.AndroidGpsLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import uk.ac.uea.locstat.MainActivity;
import uk.ac.uea.locstat.Models.Post;
import uk.ac.uea.locstat.R;

/**
 * Handles creation of posts, inserting them into the database with the framework accessor. Handles the
 * post up voting and down voting, including the maintenance of the view representing the post.
 * HAndles framework usage of location and displaying android maps.
 * Framework usage
 * -Andriod maps
 * -Location
 * -Database Accessor
 */
public class PostController extends FragmentActivity {
    Post post;
    AndroidMap gMap;
    public static ArrayList<Post> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_view);
        Intent intent = getIntent();
        this.post = posts.get(Integer.parseInt(intent.getStringExtra(MainActivity.EXTRA_MESSAGE)));
        //extracts the contents of the post, e.g. like count, timestamp, content
        TextView textView = (TextView) findViewById(R.id.testView);
        textView.setText(this.post.getContent());
        TextView textView2 = (TextView) findViewById(R.id.likeCount);
        textView2.setText("" + post.getLikes());
        TextView textView3 = (TextView) findViewById(R.id.textView3);
        textView3.setText("" + post.getTimeStamp().toString());

        //retrieiving the location of the user and starting the maps fragment to plot the location
        //of the post
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        gMap = AndroidMap.getInstance();
        gMap.initialise(mapFragment, ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap());
        gMap.setMapDefaultPosition(this.post.getLocation().getLatLng());
        gMap.getGoogleMap().getUiSettings().setMyLocationButtonEnabled(false);
        //Our current location
        gMap.addMarker(this.post.getLocation(), "test");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post_view, menu);
        return true;
    }

    /**
     * Handles when the upvote button is pressed on the post.
     */
    public void upVotePost(View view) {
        String query = "UPDATE post SET likes = likes + 1 WHERE postid =" + post.getPostId() + "";
        //updates the database using framework class
        new DatabaseAccessor().execute(query);
        TextView textView = (TextView) findViewById(R.id.likeCount);
        //updates view
        textView.setText("" + this.post.upVotePost());
        findViewById(R.id.button).setEnabled(false);
    }

    /**
     * Handles when the downvote button is pressed on the post.
     */
    public void downVotePost(View view) {
        String query = "UPDATE post SET likes = likes - 1 WHERE postid =" + post.getPostId() + "";
        //updates the database using framework class
        new DatabaseAccessor().execute(query);
        TextView textView = (TextView) findViewById(R.id.likeCount);
        //updates view
        textView.setText("" + this.post.downVotePost());
        findViewById(R.id.button4).setEnabled(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates posts from json objects.
     *
     * @param jsonObject
     * @return
     */
    public static Post jsonToPost(JSONObject jsonObject) {
        Post p = null;
        try {
            //extratcs the attributes of the json object
            int id = Integer.parseInt(jsonObject.getString("postid"));
            String content = jsonObject.getString("content");
            int likes = Integer.parseInt(jsonObject.getString("likes"));
            Timestamp timestamp = (Timestamp) jsonObject.get("datecreated");
            Location loc = new AndroidGpsLocation();
            loc.setLatitude(Float.parseFloat(jsonObject.getString("lat")));
            loc.setLongitude(Float.parseFloat(jsonObject.getString("lng")));
            //creates a new post based upon these attributes
            p = new Post(id, content, loc, null, likes, timestamp);
        } catch (Exception e) {
            System.out.println(e);
        }
        return p;
    }

    public static ArrayList<Post> createArrayListOfPosts(JSONArray output) {
        ArrayList<Post> posts = new ArrayList<>();
        for (int i = 0; i < output.length(); i++) {
            try {
                posts.add(jsonToPost(output.getJSONObject(i)));
            } catch (JSONException e) {
                System.out.println(e);
            }
        }
        return posts;
    }

    /**
     * Handles the creation of a post object.
     *
     * @param view
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws JSONException
     */
    public static void createPost(final Activity view) throws InterruptedException, ExecutionException, JSONException {
        EditText editText = (EditText) view.findViewById(R.id.editText);
        //gets the content of the message
        String message = editText.getText().toString();
        //sets the content field now empty for the user to enter a new post
        editText.setText("");
        java.util.Date utilDate = new java.util.Date();
        java.sql.Timestamp dateCreated = new java.sql.Timestamp(utilDate.getTime());
        //inserts the post into the database
        new DatabaseAccessor().execute("INSERT INTO post(content, zone, likes, datecreated, lat, lng)" +
                "VALUES('" + message + "','" + MainActivity.user.getZone().getName() + "',0, TO_TIMESTAMP('" + dateCreated + "', 'YYYY-MM-DD HH24:MI:SS')," +
                "'" + MainActivity.user.getCurrentLocation().getLatLng().latitude + "','" + MainActivity.user.getCurrentLocation().getLatLng().longitude + "')");
        Post p = MainActivity.user.newPost(message, dateCreated);
        //extracts the incremenetal post id from the post in the database
        JSONArray x = new DatabaseAccessor().execute("Select postid from post where content='" + message + "'").get();
        String y = x.getJSONObject(0).getString("postid");
        p.setPostId(Integer.parseInt(y));
        p.setLocation(MainActivity.user.getCurrentLocation());
    }
}
