package uk.ac.uea.locstat.Controllers;

import android.os.Bundle;

import com.team_toast.framework.Database.DatabaseAccessor;
import com.team_toast.framework.FileIO.InternalFileImp;
import com.team_toast.framework.FileIO.SimpleText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import uk.ac.uea.locstat.MainActivity;
import uk.ac.uea.locstat.Models.Post;
import uk.ac.uea.locstat.R;

/**
 * PreviousPostController, handles the users previous posts by loading them from file into memory
 * and saves posts to file. Extends main activity to allow code reuse.
 *
 * Framework usage
 * -
 */
public class PreviousPostController extends MainActivity {
    //static ArrayList<Post> postsOnFile;
    @Override
    public void onResume() {
        super.onResume();
        //loads posts from file and adds to the users post history
        user.getUserPostHistory().addAll(loadPrevPostsFromFile());
        this.postsToDisplay = user.getUserPostHistory();
        this.displayAsListView(user.getUserPostHistory());
        //saves the posts to file
        savePosts();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_posts);
        onResume();
    }

    /**
     * Saves post ids to file. This maintains the anonymity by not storing the whole post.
     */
    public void savePosts() {
        JSONArray postsToSave = new JSONArray();
        for (Post p : user.getUserPostHistory()) {
            JSONObject jsonObject = new JSONObject();
            try {
                //get the id of each post and place in a json object, place this in a json array
                jsonObject.put("postid", p.getPostId());
            } catch (Exception e) {

            }
            postsToSave.put(jsonObject);
        }
        //calls framework file io to save this string json to file
        SimpleText test = new SimpleText(new InternalFileImp(this));
        test.setBody(postsToSave.toString());
        test.writeToFile("post.hist");

       // postsOnFile = user.getUserPostHistory();
        //empty memory to remvoe any duplicates or post overwrites
        user.setUserPostHistory(new ArrayList<Post>());
    }

    /**
     * Loads all the posts from their id on file into posts in memory.
     * @return
     */
    public ArrayList<Post> loadPrevPostsFromFile() {
        ArrayList<Post> savedPosts = new ArrayList<>();
        //create a framework
       SimpleText st = new SimpleText(new InternalFileImp(this));
       st.readFromFile("post.hist");
       String jsonStr = st.formatOutput();
        //loads post ids from file as a json string.
        try {
            JSONArray loadedIds = new JSONArray(jsonStr);
            for (int i = 0; i < loadedIds.length(); i++) {
                try {
                    //with loaded ids select the posts to get the most update state of the post, eg up vote count
                    savedPosts.add(PostController.jsonToPost(new DatabaseAccessor().execute("select * from post where postid="
                            + loadedIds.getJSONObject(i).getString("postid")+" order by datecreated desc").get().getJSONObject(0)));
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        } catch (JSONException e) {
            System.out.println(e);
        }
        return savedPosts;
    }
}
