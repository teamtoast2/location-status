package uk.ac.uea.locstat.Models;

import com.team_toast.framework.Location.Location;

import java.sql.Timestamp;
import java.util.ArrayList;



/**
 * Created by qrk13cdu on 17/11/2015.
 */
public class User {
    private ArrayList<Post> userPostHistory;
    private Location currentLocation;
    private Zone zone;

    public User() {
        this.currentLocation = null;//get location
        this.userPostHistory = new ArrayList<>();
        this.zone = new Zone("Uncategorised location");

    }

    public ArrayList<Post> getUserPostHistory() {
        return userPostHistory;
    }

    public Post newPost(String postContent, Timestamp dateCreated) {
        Post p = new Post(postContent, this.currentLocation, this.zone, dateCreated);
        this.userPostHistory.add(p);
        return p;
    }

    public void setUserPostHistory(ArrayList<Post> userPostHistory) {
        this.userPostHistory = userPostHistory;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public void setZone(Zone currentZone) {
        this.zone = currentZone;
    }

    public Zone getZone(){
        return this.zone;
    }
}
