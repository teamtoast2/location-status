package uk.ac.uea.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import uk.ac.uea.framework.FrameworkCopyright;

public class DisplayMessageActivity extends AppCompatActivity {

    public AppCopyright appCopyright;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_display_message);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
// Get the message from the intent
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
// Create the text view
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);
// Set the text view as the activity layout
        //setContentView(textView);

        appCopyright = new AppCopyright();
        TextView textViewCopy = (TextView) findViewById(R.id.copyRightView);
        textViewCopy.setTextSize(40);
        textViewCopy.setText(appCopyright.getAppCopyright());



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
