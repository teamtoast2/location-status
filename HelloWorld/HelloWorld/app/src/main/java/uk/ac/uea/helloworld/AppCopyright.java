package uk.ac.uea.helloworld;

/**
 * Created by qrk13cdu on 14/10/2015.
 */
import uk.ac.uea.framework.FrameworkCopyright;

public class AppCopyright extends FrameworkCopyright {

    public String getAppCopyright(){
        return "This application is created by Joe Lilley and James Rogers, (c) 2015";
    }
}
