package uk.ac.uea.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;

import uk.ac.uea.framework.Music;
import uk.ac.uea.framework.SoundResource;
import uk.ac.uea.framework.Audio;
import uk.ac.uea.framework.implementation.AndroidAudio;


public class MainActivity extends AppCompatActivity {

    public SoundResource soundResource;

    public final static String EXTRA_MESSAGE = "uk.ac.uea.helloworld.MESSAGE";
    /** Called when the user clicks the Send button */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

        soundResource.play();
    }

    public void playSound(View view) {

        Audio MyAudio = new AndroidAudio(this);
        Music MyMusic = MyAudio.createMusic("musicfile1.mp3");
        Music MyMusic2 = MyAudio.createMusic("musicfile2.mp3");
        MyMusic.play();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        soundResource=new SoundResource(this);
        soundResource.load("click.mp3");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
