package uk.ac.uea.framework;

/**
 * FrameworkCopyright
 *@author qrk13cdu
 *@since 2015-10-14
 *@version 1.0
 * Displays copyright information for applications using this framework.
 * Provides abstract method for easy adding additional copyright.
 */
public abstract class FrameworkCopyright {
    private String copyrightText = "This application is based on the Simple Android Application Framework. (c) Team Toast 2015.";

    /**
     * Method to get the copyright information.
     * @return Formatted representation of copyright information.
     */
    public final String getCopyright() {
        return copyrightText + "\n\n" + getAppCopyright();
    }

    /**
     * Abstract method that must be implemented to add additional copyright information.
     * @return Additional copyright.
     */
    protected abstract String getAppCopyright();

}
